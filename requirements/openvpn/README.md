# Ansible Role: OpenVPN

Role para instalação e configuração do OpenVPN.

## Pré-requisitos

Esta role depende do pacote `easy-rsa`; quando executada no RHEL/CentOS, o repositório
EPEL deve ser previamente instalado e configurado.

## Variáveis

Abaixo são descritas as principais variáveis da role. Para maiores detalhes, veja
o arquivo [`defaults/main.yml`](defaults/main.yml).

```yaml
openvpn_server_ip: 123.45.6.7
openvpn_server_port: 1194
openvpn_server_protocol: udp
```

Configurações de IP, porta e protocolo do servidor.

## Dependências

- EPEL (quando executada no RHEL/CentOS)

## Exemplo de uso

Arquivo `requirements.yml`:

```yaml
- src: git@gitlab.com:mandic-labs/ansible/roles/epel.git
  scm: git
  version: v0.1.0
  name: epel
  
- src: git@gitlab.com:mandic-labs/ansible/roles/openvpn.git
  scm: git
  version: v0.2.0
  name: openvpn
```

Playbook:

```yaml
---
- hosts: all
  roles:
    - role: epel
      vars:
        epel_enable_repo: true
        
- hosts: all
  roles:
    - role: openvpn
      vars:
        openvpn_server_port: 1194
        openvpn_server_protocol: udp
```

Inicialização dos requirements:
```
ansible-galaxy install -r requirements.yml
```
Caso queira instalar eles dentro do diretório/repositório atual:

```
ansible-galaxy install -r requirements.yml -p ./requirements
```
Lembre-se de alterar seu `playbook.yml` pra passar o diretório das roles (ex: `role: requirements/openvpn`).


Execução da playbook:
```
ansible-playbook playbook.yml
```
## Licença

Copyright © 2019 Mandic Cloud Solutions. Todos os direitos reservados.
